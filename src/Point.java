import edu.princeton.cs.algs4.StdDraw;

import java.util.Comparator;

/**
 * Created by anton.kuritsyn on 22.10.2016.
 */
public class Point implements Comparable<Point> {

    private final int x;
    private final int y;

    @Override
    public int compareTo(Point p) {
        if (y < p.y) {
            return -1;
        } else if (y == p.y) {
            if (x < p.x) {
                return -1;
            } else if (x == p.x) {
                return 0;
            }
        }
        return 1;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void draw() {
        StdDraw.point(x, y);
    }

    public void drawTo(Point that) {
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public double slopeTo(Point p) {
        double dx = p.x - x;
        double dy = p.y - y;

        if (dx == 0 && dy == 0) {
            return Double.NEGATIVE_INFINITY;
        }

        if (dx == 0) {
            return Double.POSITIVE_INFINITY;
        }

        if (dy == 0) {
            return +0;
        }

        return dy / dx;
    }

    public Comparator<Point> slopeOrder() {
        return new PointComparator();
    }

    private class PointComparator implements Comparator<Point> {
        @Override
        public int compare(Point p1, Point p2) {
            double slope1 = slopeTo(p1);
            double slope2 = slopeTo(p2);
            if (slope1 < slope2) {
                return -1;
            }
            if (slope1 > slope2) {
                return 1;
            }
            return 0;
        }
    };

    public static void main(String[] args) {

        Point p1 = new Point(2, 2);
        Point p2 = new Point(2, 4);

        System.out.println(p1.slopeTo(p2));

    }
}