import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.*;

/**
 * Created by anton.kuritsyn on 22.10.2016.
 */
public class FastCollinearPoints {

    private final List<LineSegment> segments = new ArrayList<>();
    private final Map<Double, List<Point>> slopeToPoints = new HashMap<>();
    private final List<Point> lastArray = new ArrayList<>();

    public FastCollinearPoints(Point[] points) {
        if (points == null) {
            throw new NullPointerException();
        }

        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                if (points[i].compareTo(points[j]) == 0) {
                    throw new IllegalArgumentException();
                }
            }
        }

        for (int i = 0; i < points.length - 3; i++) {
            Point p = points[i];
            Comparator<Point> slopeOrder = p.slopeOrder();
            Arrays.sort(points, i + 1, points.length, slopeOrder);
            double currentSlope = Double.NaN;

            for (int j = i + 1; j < points.length; j++) {
                if (p.slopeTo(points[j]) == currentSlope) {
                    lastArray.add(points[j]);
                    if (j == points.length - 1) {
                        addSegment();
                    }
                } else {
                    addSegment();
                    currentSlope = p.slopeTo(points[j]);

                    if (lastArray.size() != 1) {
                        lastArray.clear();
                        lastArray.add(p);
                        lastArray.add(points[j]);
                    }
                }
            }
        }

        slopeToPoints.clear();
        lastArray.clear();
    }

    private void addSegment() {
        if (lastArray.size() >= 4) {
            double slope = lastArray.get(0).slopeTo(lastArray.get(1));
            List<Point> points = slopeToPoints.get(slope);
            if (points != null) {
                for (Point p1 : points) {
                    for (Point p : lastArray) {
                        if (p.compareTo(p1) == 0) {
                            return;
                        }
                    }
                }
            } else {
                points = new ArrayList<>();
                slopeToPoints.put(slope, points);
            }
            Point[] array = lastArray.toArray(new Point[lastArray.size()]);
            Arrays.sort(array, (Point p1, Point p2) -> p1.compareTo(p2));
            segments.add(new LineSegment(array[0], array[array.length - 1]));
            points.addAll(lastArray);
        }
    }

    public int numberOfSegments() {
        return segments.size();
    }

    public LineSegment[] segments() {
        return segments.toArray(new LineSegment[segments.size()]);
    }

    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        long time = System.currentTimeMillis();
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        time = System.currentTimeMillis() - time;
        System.out.println("Running time " + time);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }

}