import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by anton.kuritsyn on 22.10.2016.
 */
public class BruteCollinearPoints {

    private final List<LineSegment> segments = new ArrayList<>();

    public BruteCollinearPoints(Point[] points) {
        if (points == null) {
            throw new NullPointerException();
        }

        Arrays.sort(points, (Point p1, Point p2) -> p1.compareTo(p2));

        for (int i = 0; i < points.length - 3; i++) {
            Point pivot = points[i];
            for (int j = i + 1; j < points.length - 2; j++) {
                double slope1 = pivot.slopeTo(points[j]);
                for (int k = j + 1; k < points.length - 1; k++) {
                    if (slope1 != pivot.slopeTo(points[k])) {
                        continue;
                    }
                    for (int l = k + 1; l < points.length; l++) {
                        if (slope1 != pivot.slopeTo(points[l])) {
                            continue;
                        }
                        segments.add(new LineSegment(points[i], points[l]));
                    }
                }
            }
        }
    }

    public int numberOfSegments() {
        return segments.size();
    }

    public LineSegment[] segments() {
        return segments.toArray(new LineSegment[segments.size()]);
    }

    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        long time = System.currentTimeMillis();
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        time = System.currentTimeMillis() - time;
        System.out.println("Running time " + time);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }

}